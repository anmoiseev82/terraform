#-------------------------------------------------------------
#
# My Terraform server
#
# Made by Anton Moiseev
#
#-------------------------------------------------------------

provider "aws" {
  region = "us-east-2"
}

resource "aws_instance" "My_WebServer" {
  ami                    = "ami-0231217be14a6f3ba" # Amazon Linux instance ami
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.My_WebServer.id]
  user_data              = <<EOF
#!/bin/bash
sudo yum -y update
sudo yum install -y httpd httpd-tools mod_ssl
myip=`curl http://169.254.169.254/latest/meta-data/local-ipv4`
echo "<h2>WebServer with IP: $myip</h2><br>Build by Terraform!" > /var/www/html/index.html
sudo systemctl enable httpd
sudo systemctl start httpd
sudo chkconfig httpd on
EOF

  tags = {
    Name  = "My Server from Terrafor"
    Owner = "Anton Moiseev"
  }
}

resource "aws_security_group" "My_WebServer" {
  name        = "Web server Security group"
  description = "My First security group"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "My Server from Terrafor"
    Owner = "Anton Moiseev"
  }
}
