variable "region" {
  description = "Please enter AWS region"
  type        = string
  default     = "us-east-1"
}

variable "instance_type" {
  description = "Enter Instance Type"
  type        = string
  default     = "t2.micro"
}

variable "allow_ports" {
  description = "List of ports to open for server"
  type        = list(any)
  default     = ["80", "443", "22", "8080"]
}

variable "common_tags" {
  description = "Common tags to apply to all resources"
  type        = map(any)
  default = {
    Owner       = "Anton Moiseev"
    Project     = "Phoenix"
    CostCenral  = "12345"
    Environment = "development"
  }
}
