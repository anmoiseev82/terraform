output "My_instance_id" {
  value = aws_instance.My_WebServer_web.id
}

output "My_server_ip" {
  value = aws_eip.My_static_IP.public_ip
}

output "My_sg_id" {
  value = aws_security_group.My_WebServer.id
}
