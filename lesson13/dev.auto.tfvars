# Auto fill parametrs for DEV

#File can be name:
# terraform.tfvars
# *.auto.tfvars
# f.e. prod.auto.tfvars, dev.auto.tfvars

region        = "us-east-1"
instance_type = "t2.micro"

allow_ports = ["80", "443", "22", "8080"]

common_tags = {
  Owner       = "Anton Moiseev"
  Project     = "Phoenix"
  CostCenral  = "12345"
  Environment = "dev"
}
