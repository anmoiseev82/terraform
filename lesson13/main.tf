#-------------------------------------------------------------
#
# My Terraform server
#
# Made by Anton Moiseev
#
#-------------------------------------------------------------

provider "aws" {
  region = var.region
}

data "aws_ami" "latest_amazon" {
  owners      = ["amazon"]
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*-x86_64-gp2"]
  }
}

resource "aws_eip" "My_static_IP" {
  instance = aws_instance.My_Server_web.id
  tags     = merge(var.common_tags, { Name = "${var.common_tags["Environment"]} Server IP" })
}

resource "aws_instance" "My_Server_web" {
  ami                    = data.aws_ami.latest_amazon.id
  instance_type          = var.instance_type
  vpc_security_group_ids = [aws_security_group.My_WebServer.id]
  tags                   = merge(var.common_tags, { Name = "Server Build by Terraform" })
  /* tags = {
    Name    = "Server Build by Terraform"
    Owner   = "Anton Moiseev"
    Project = "Phoenix"
  } */
}

resource "aws_security_group" "My_WebServer" {
  name        = "Web server Security group"
  description = "My First security group"

  dynamic "ingress" {
    for_each = var.allow_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge(var.common_tags, { Name = "Server Security_group" })
}
