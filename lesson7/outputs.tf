output "webserver_instance_id" {
  value = aws_instance.My_WebServer.id
}

output "webserver_public_ip" {
  value = aws_eip.My_static_IP.public_ip
}

output "webserver_security_group_id" {
  value = aws_security_group.My_WebServer.id
}

output "webserver_security_group_arn" {
  value = aws_security_group.My_WebServer.arn
}
