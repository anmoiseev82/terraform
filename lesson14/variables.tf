variable "environment" {
  default = "DEV"
}

variable "project_name" {
  default = "ANBESA"
}

variable "owner" {
  default = "Anton Moiseev"
}
