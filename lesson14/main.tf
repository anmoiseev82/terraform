#-------------------------------------------
# Terraform lessons
#
# Local Variables
#
#
#
#  Made by Anton Moiseev
#
#-----------------------------------------

provider "aws" {}

data "aws_region" "current" {}
data "aws_availability_zones" "available" {}

locals {
  full_project_name = "${var.environment}-${var.project_name}"
  project_owner     = "${var.owner} owner by ${var.project_name}"
  region            = data.aws_region.current.description
  az_list           = join(",", data.aws_availability_zones.available.names)
  location          = "In region ${local.region} available AZ ${local.az_list}"
}

resource "aws_eip" "my_static_ip" {
  tags = {
    Name          = "Static IP"
    Owner         = var.owner
    Project       = local.full_project_name
    Project_owner = local.project_owner
    Region_az     = local.az_list
    Location      = local.location
  }
}
