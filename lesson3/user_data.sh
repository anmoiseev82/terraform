#!/bin/bash
sudo yum -y update
sudo yum install -y httpd httpd-tools mod_ssl
myip=`curl http://169.254.169.254/latest/meta-data/local-ipv4`
echo "<h2>WebServer with IP: $myip</h2><br>Build by Terraform! Using external script" > /var/www/html/index.html
sudo systemctl enable httpd
sudo systemctl start httpd
sudo chkconfig httpd on
