#-------------------------------------------------------------
#
# My Terraform server
#
# Made by Anton Moiseev
#
#-------------------------------------------------------------

provider "aws" {
  region = "us-east-2"
}

resource "aws_instance" "My_WebServer" {
  ami                    = "ami-0231217be14a6f3ba" # Amazon Linux instance ami
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.My_WebServer.id]
  user_data = templatefile("user_data.sh.tpl", {
    f_name = "Anton",
    l_name = "Moiseev",
    names  = ["Kolya", "Vasya", "Petya", "Bob", "John", "Valerya Antonovna Moiseeva"]
  })
  tags = {
    Name  = "My Server from Terrafor"
    Owner = "Anton Moiseev"
  }
}

resource "aws_security_group" "My_WebServer" {
  name        = "Web server Security group"
  description = "My First security group"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "My Server from Terrafor"
    Owner = "Anton Moiseev"
  }
}
