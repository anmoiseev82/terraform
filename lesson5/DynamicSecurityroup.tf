#-------------------------------------------------------------
#
# My Terraform server
#
# Made by Anton Moiseev
#
#-------------------------------------------------------------

provider "aws" {
  region = "us-east-2"
}

resource "aws_security_group" "My_WebServer" {
  name = "Dynamic Security group"

  dynamic "ingress" {
    for_each = ["22", "80", "443", "8080", "1541", "9092"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
