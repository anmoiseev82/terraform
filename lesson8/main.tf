#-------------------------------------------------------------
#
# My Terraform server
#
# Made by Anton Moiseev
#
#-------------------------------------------------------------

provider "aws" {
  region = "us-east-2"
}

resource "aws_eip" "My_static_IP" {
  instance = aws_instance.My_WebServer.id
}


resource "aws_instance" "My_Server_web" {
  ami                    = "ami-0231217be14a6f3ba" # Amazon Linux instance ami
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.My_WebServer.id]

  tags = {
    Name = "Server-Web"
  }
  depends_on = [aws_instance.My_Server_db, aws_instance.My_Server_app]
}

resource "aws_instance" "My_Server_app" {
  ami                    = "ami-0231217be14a6f3ba" # Amazon Linux instance ami
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.My_WebServer.id]

  tags = {
    Name = "Server-Applycation"
  }
  depends_on = [aws_instance.My_Server_db]
}

resource "aws_instance" "My_Server_db" {
  ami                    = "ami-0231217be14a6f3ba" # Amazon Linux instance ami
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.My_WebServer.id]

  tags = {
    Name = "Server-Database"
  }
}

resource "aws_security_group" "My_WebServer" {
  name        = "Web server Security group"
  description = "My First security group"

  dynamic "ingress" {
    for_each = ["80", "443", "22"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "My Server from Terrafor"
    Owner = "Anton Moiseev"
  }
}
